# Terraform code to create a K8s cluster in Azure with application gateway and ingress

## Pre-requisites

The following items should be installed in your system:

* Java 8 or newer.
* git command line tool (https://help.github.com/articles/set-up-git)
* Your preferred IDE
  * Eclipse with the terraform plugin.
  not there, just follow the install process here: https://www.eclipse.org/m2e/
  * [Spring Tools Suite](https://spring.io/tools) (STS)
  * IntelliJ IDEA
  * [VS Code](https://code.visualstudio.com)
* Terraform
* AZ CLI - Optional
* Helm - Optional

## Variables to be set before running the pipeline

* AZ_SUBSCRIPTION_ID
* AZ_CLIENT_ID
* AZ_SECRET_ID
* AZ_TENANT_ID
* AZ_CLIENT_OBJECT_ID
