variable "resource_group_name" {}
variable "location" {}
variable "billing_tag" {}
variable "environment" {}
variable "service_principal_id" {}
variable "service_principal_secret" {}
variable "service_principal_object_id" {}
