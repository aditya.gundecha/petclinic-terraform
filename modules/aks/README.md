
# Azure kubernetes servics

| Inputs                                | Required/Optional | Description                                                       | Default value                            |
| ------------------------------------- | ----------------- | ----------------------------------------------------------------- | ---------------------------------------- |
| `resource_group_name`                 | Required          | Resource Group Name                                               |                                          |
| `location`                            | Required          | Location                                                          | `West Europe`                            |
| `aks_service_principal_app_id`        | Required          | Application ID/Client ID  of the service principal                |                                          |
| `aks_service_principal_client_secret` | Required          | Secret of the service principal                                   |                                          |
| `aks_service_principal_object_id`     | Required          | Object ID of the service principal                                |                                          |
| `virtual_network_name`                | Optional          | Virtual network name                                              | `aksVirtualNetwork`                      |
| `virtual_network_address_prefix`      | Optional          | Containers DNS server IP address                                  | `172.20.0.0/16`                          |
| `aks_subnet_name`                     | Optional          | AKS Subnet Name                                                   | `kubesubnet`                             |
| `aks_subnet_address_prefix`           | Optional          | Containers DNS server IP address                                  | `172.20.20.0/24`                         |
| `app_gateway_subnet_address_prefix`   | Optional          | Containers DNS server IP address                                  | `172.20.10.0/24`                         |
| `app_gateway_name`                    | Optional          | Name of the Application Gateway                                   | `ApplicationGateway`                     |
| `app_gateway_sku`                     | Optional          | Name of the Application Gateway SKU                               | `Standard_v2`                            |
| `aks_name`                            | Optional          | Name of the AKS cluster                                           | `aks-cluster`                            |
| `aks_dns_prefix`                      | Optional          | DNS prefix to use with hosted Kubernetes API server FQDN          | `aks`                                    |
| `aks_agent_os_disk_size`              | Optional          | Disk size (in GB) to provision for each of the agent pool nodes   | `40`                                     |
| `aks_agent_vm_size`                   | Optional          | The size of the Virtual Machine                                   | `Standard_D3_v2`                         |
| `kubernetes_version`                  | Optional          | The version of Kubenertes                                         | `1.17.11`                                |
| `aks_service_cidr`                    | Optional          | A CIDR notation IP range from which to assign service cluster IPs | `10.0.0.0/16`                            |
| `aks_dns_service_ip`                  | Optional          | Containers DNS server IP address                                  | `10.0.0.10`                              |
| `aks_docker_bridge_cidr`              | Optional          | A CIDR notation IP for Docker bridge                              | `172.17.0.1/16`                          |
| `aks_enable_rbac`                     | Optional          | Enable RBAC on the AKS cluster                                    | `false`                                  |
| `vm_user_name`                        | Optional          | User name for the VM                                              | `vmuser1`                                |
| `public_ssh_key_path`                 | Optional          | Public key path for SSH                                           | `~/.ssh/id_rsa.pub`                      |
| `app_gateway_subnet_name`             | Optional          | Name of the app gateway subnet                                    | `appgwsubnet`                            |
| `app_gateway_ingress_name`            | Optional          | Name of application gateway ingress                               | `application-gateway-kubernetes-ingress` |
| `tags`                                | Optional          | Tags                                                              | `{source = "terraform"}`                 |

## Usage

```hcl-terraform
module "appgw-ingress-k8s-cluster" {
  source                              = "Azure/appgw-ingress-k8s-cluster/azurerm"
  version                             = "0.1.0"
  resource_group_name                 = azurerm_resource_group.test.name
  location                            = "west europe"
  aks_service_principal_app_id        = "<App ID of the service principal>"
  aks_service_principal_client_secret = "<Client secret of the service principal>"
  aks_service_principal_object_id     = "<Object ID of the service principal>"

  tags = {
    environment = "dev"
    costcenter  = "it"
  }
}
```
