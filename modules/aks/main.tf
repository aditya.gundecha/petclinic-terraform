# # Locals block for names.    join("-", ["pip", local.project_name_prefix])
locals {
  backend_address_pool_name      = join("-", [azurerm_virtual_network.this.name, "beap"]) 
  frontend_port_name             = join("-", [azurerm_virtual_network.this.name, "feport"])
  frontend_ip_configuration_name = join("-", [azurerm_virtual_network.this.name, "feip"])
  http_setting_name              = join("-", [azurerm_virtual_network.this.name, "be-htst"])
  listener_name                  = join("-", [azurerm_virtual_network.this.name, "httplstn"])
  request_routing_rule_name      = join("-", [azurerm_virtual_network.this.name, "rqrt"])
  project_name_prefix            = "petclinic"
}

#Resources
resource "azurerm_resource_group" "this" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

#Virtual Networks
resource "azurerm_virtual_network" "this" {
  name                = var.virtual_network_name
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  address_space       = [var.virtual_network_address_prefix]
  tags                = var.tags
}

resource "azurerm_subnet" "kubesubnet" {
  name                 = "kubesubnet"
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefix       = var.aks_subnet_address_prefix
}

resource "azurerm_subnet" "appgwsubnet" {
  name                 = "appgwsubnet"
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefix       = var.app_gateway_subnet_address_prefix
}

# Public Ip
resource "azurerm_public_ip" "this" {
  name                = join("-", ["pip", local.project_name_prefix])
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  allocation_method   = "Static"
  sku                 = "Standard"

  tags = var.tags
}

#Application Gateway
resource "azurerm_application_gateway" "this" {
  name                = var.app_gateway_name
  resource_group_name = azurerm_resource_group.this.name
  location            = azurerm_resource_group.this.location

  sku {
    name     = var.app_gateway_sku
    tier     = "Standard_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "appGatewayIpConfig"
    subnet_id = azurerm_subnet.appgwsubnet.id
  }

  frontend_port {
    name = local.frontend_port_name
    port = 80
  }

  frontend_port {
    name = "httpsPort"
    port = 443
  }

  frontend_ip_configuration {
    name                 = local.frontend_ip_configuration_name
    public_ip_address_id = azurerm_public_ip.this.id
  }

  backend_address_pool {
    name = local.backend_address_pool_name
  }

  backend_http_settings {
    name                  = local.http_setting_name
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 1
  }

  http_listener {
    name                           = local.listener_name
    frontend_ip_configuration_name = local.frontend_ip_configuration_name
    frontend_port_name             = local.frontend_port_name
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = local.request_routing_rule_name
    rule_type                  = "Basic"
    http_listener_name         = local.listener_name
    backend_address_pool_name  = local.backend_address_pool_name
    backend_http_settings_name = local.http_setting_name
  }

  tags = var.tags

  depends_on = [
    azurerm_virtual_network.this,
    azurerm_public_ip.this,
  ]
}

#AKS
resource "azurerm_kubernetes_cluster" "aks" {
  name       = var.aks_name
  location   = azurerm_resource_group.this.location
  dns_prefix = var.aks_dns_prefix

  resource_group_name = azurerm_resource_group.this.name

  linux_profile {
    admin_username = var.vm_user_name

    ssh_key {
      key_data = file(var.public_ssh_key_path)
    }
  }

  addon_profile {
    http_application_routing {
      enabled = false
    }
  }

  default_node_pool {
    name            = "agentpool"
    node_count      = var.aks_agent_count
    vm_size         = var.aks_agent_vm_size
    os_disk_size_gb = var.aks_agent_os_disk_size
    vnet_subnet_id  = azurerm_subnet.kubesubnet.id
  }

  service_principal {
    client_id     = var.aks_service_principal_app_id
    client_secret = var.aks_service_principal_client_secret
  }

  network_profile {
    network_plugin     = "azure"
    dns_service_ip     = var.aks_dns_service_ip
    docker_bridge_cidr = var.aks_docker_bridge_cidr
    service_cidr       = var.aks_service_cidr
  }

  depends_on = [
    azurerm_virtual_network.this,
    azurerm_application_gateway.this,
  ]
  tags = var.tags
}
