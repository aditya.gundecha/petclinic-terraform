provider "helm" {
  kubernetes {
    load_config_file       = "false"
    host                   = azurerm_kubernetes_cluster.aks.kube_config.0.host
    username               = azurerm_kubernetes_cluster.aks.kube_config.0.username
    password               = azurerm_kubernetes_cluster.aks.kube_config.0.password
    client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config.0.cluster_ca_certificate)
  }
}

resource "helm_release" "this" {
  name         = var.app_gateway_ingress_name
  repository   = "https://appgwingress.blob.core.windows.net/ingress-azure-helm-package/"
  chart        = "ingress-azure"
  version      = "1.2.0-rc3"
  force_update = true
  values = [
    file("${path.module}/values.yaml")
  ]

  set {
    name  = "appgw.resourceGroup"
    value = var.resource_group_name
  }

  set {
    name  = "appgw.name"
    value = var.app_gateway_name
  }

  set {
    name  = "appgw.subscriptionId"
    value = "8867461d-7770-4c24-b499-24e825b0545c"
  }

  set {
    name  = "appgw-name"
    value = "ApplicationGateway1"
  }

  depends_on = [azurerm_kubernetes_cluster.aks]
}
