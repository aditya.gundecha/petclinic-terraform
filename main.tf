locals {
  tags = {
    "billing_tag" = var.billing_tag
    "Environment" = var.environment
    "ManagedBy"   = "Terraform"
    "CreatedOn"   = formatdate("MMM DD, YYYY hh:mm ZZZ", timestamp())
  }
}


# AKS with Application Gateway
module "appgw-ingress-k8s-cluster" {
  source                              = "./modules/aks"
  resource_group_name                 = var.resource_group_name
  location                            = var.location
  aks_service_principal_app_id        = var.service_principal_id
  aks_service_principal_client_secret = var.service_principal_secret
  aks_service_principal_object_id     = var.service_principal_object_id
  tags                                = local.tags
}
